
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilTriListe;

public class Question01 {
    
   public static void main(String[] args) {
         
      System.out.println("\nListe des membres du Club \n");
        
       String format="%-10s %-10s%-2s %3d kg %-15s %2d Victoires\n";
       
       UtilTriListe.trierLesPersonnesParNom(ClubJudo.listeDesMembres);
       
       for(Personne pers : ClubJudo.listeDesMembres){
            
          System.out.printf(format,pers.nom,pers.prenom,pers.sexe,pers.poids,pers.ville,pers.nbVictoires);        
       } 
        
      System.out.println();
    }
}






