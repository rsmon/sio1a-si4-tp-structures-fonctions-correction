
package programmes;


import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;

public class Question06 {

    public static void main(String[] args) {
          
       int mini=1000;
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("M")&&pers.poids<mini) {
            
              mini=pers.poids;  
       
          }   
       } 
        
        System.out.printf("\n Poids le faible parmi les judokas hommes:%3d kg\n\n",mini);
       
    }
}

