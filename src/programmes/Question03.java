
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilTriListe;

public class Question03 {

    public static void main(String[] args) {
         
       System.out.println("\n Liste des membres masculins du Club pesant plus de 80 kg\n");
        
       String format=" %-10s %-10s %3d kg %-10s\n";
       
       UtilTriListe.trierLesPersonnesParNom(ClubJudo.listeDesMembres);
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("M") && pers.poids>80) {
            
            System.out.printf(format,pers.nom,pers.prenom, pers.poids,pers.ville);        
       
          }   
       } 
        
      System.out.println();
              
    }
}

