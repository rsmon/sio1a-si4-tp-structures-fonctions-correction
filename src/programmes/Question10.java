package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilDojo;

public class Question10 {

  public static void main(String[] args) {
     
      int nbCateg=UtilDojo.categories.length;
    
      int[][] effectif= new int[2][nbCateg];
      
//<editor-fold  desc="ICI VOUS DEVEZ ECRIRE LE CODE QUI PERMET DE REMPLIR LE TABLEAU effectif A PARTIR DE LA LISTE Util.Dojo.membres">
      
      for (Personne pers : ClubJudo.listeDesMembres){
          
          int indiceSexe  = pers.sexe.equals("F")?0:1;
          int indiceCateg = UtilDojo.determineIndiceCategorie(pers.sexe, pers.poids);
          
          effectif[indiceSexe][indiceCateg]++;
      }
      
//</editor-fold>
   
      
      System.out.println("\n Répartition des effectifs par sexes et catégories de poids\n");
      System.out.println("   slg mlg  lg mmy  my mld  ld ");
      System.out.println();
      for(int iSexe=0;iSexe<2;iSexe++){
     
          if(iSexe==0)System.out.print(" f "); else System.out.print(" h ");
          
          for(int iCateg=0;iCateg<nbCateg;iCateg++ ){
          
              System.out.printf("%3d ",effectif[iSexe][iCateg]);
          }
          System.out.println(); 
      }
      System.out.println();
  }
}





