
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;

public class Question04 {

    public static void main(String[] args) {
         
       System.out.println("\n Effectif du club:\n");
        
       int cptH=0, cptF=0;
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("M")) cptH++; else cptF++;
            
       }  
       
       System.out.printf(" Effectif masculin: %2d\n",cptH);
       System.out.printf(" Effectif féminin:  %2d\n",cptF);
       System.out.println();
       System.out.printf(" Effectif total:    %2d\n",cptH+cptF);
       System.out.println();
    }
}

