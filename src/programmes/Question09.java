package programmes;

import leclubdejudo.ClubJudo;

import leclubdejudo.Personne;
import utilitaires.UtilDate;
import utilitaires.UtilDojo;

public class Question09 {

  public static void main(String[] args) {
     
    float moyenneAges;
    int   nbMembres=0;
    int   totalAges=0;
    
    for(Personne pers : ClubJudo.listeDesMembres){

        String categ=UtilDojo.determineCategorie(pers.sexe, pers.poids);
        
        if( categ.equals("légers")){
      
            int agePers            = UtilDate.ageEnAnnees(pers.dateNaiss);
            totalAges+=agePers;
            nbMembres++;
        }
    } 
    
    moyenneAges=(float)totalAges/nbMembres;
    
    System.out.printf("\n Moyenne d'age des membres du club de catégorie légers: %2.1f ans\n\n ",moyenneAges);
   }
}



