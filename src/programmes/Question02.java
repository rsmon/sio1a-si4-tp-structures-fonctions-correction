
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilTriListe;

public class Question02 {

    public static void main(String[] args) {
         
       System.out.println("\n Liste des membresdu Club de sexe féminins\n");
           
       UtilTriListe.trierLesPersonnesParNom(ClubJudo.listeDesMembres);
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("F")) {
            
              String format=" %-10s %-10s\n";
              System.out.printf(format,pers.nom,pers.prenom);          
       
          }   
       } 
        
      System.out.println();
              
    }

    
}

