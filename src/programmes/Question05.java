
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;

public class Question05 {

    public static void main(String[] args) {
          
       int   cptF=0;
       int   totalPoids=0;
       float moyenne;
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("F")) {
            
              cptF++; totalPoids+=pers.poids;     
       
          }   
       } 
        
       moyenne=(float)totalPoids/cptF;
       System.out.printf("\n Moyenne des poids des judokas féminins: %2.2f kg\n\n", moyenne);
    }
}

