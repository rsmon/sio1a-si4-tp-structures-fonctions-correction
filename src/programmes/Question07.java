
package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;

public class Question07 {

    public static void main(String[] args) {
          
                
       Personne lePlusLeger=ClubJudo.listeDesMembres.get(0);
       
       int mini=lePlusLeger.poids;
       
       for(Personne pers : ClubJudo.listeDesMembres){
           
          if( pers.sexe.equals("M")&&pers.poids<mini) {
            
              mini=pers.poids; lePlusLeger=pers;  
       
          }   
       } 
       
       System.out.print("\n Judoka masculin le plus léger: ");    
       
       String format="%-6s %-8s %4d kg \n\n";
       System.out.printf(format,lePlusLeger.prenom,lePlusLeger.nom, lePlusLeger.poids); 
       
      
    }
}



