package programmes;

import leclubdejudo.ClubJudo;
import leclubdejudo.Personne;
import utilitaires.UtilDate;
import utilitaires.UtilDojo;

public class Question08 {

  public static void main(String[] args) {
     
    String aujourdhui = UtilDate.aujourdhuiChaine();  
    System.out.printf("\n Liste des membres masculins de la catégorie légers agés de 20 ans ou plus au: %10s\n\n",aujourdhui);
        
    String format=" %-10s %-10s %-10s %2d ans %3d kg %-20s\n";
        
    for(Personne pers : ClubJudo.listeDesMembres){
       
        int agePers            = UtilDate.ageEnAnnees(pers.dateNaiss);
        String categPers       = UtilDojo.determineCategorie(pers.sexe, pers.poids);    
        
        if( pers.sexe.equals("M")&& agePers>20 && categPers.equals("légers") ){ 
           
           String dateNaiss       = UtilDate.formate(pers.dateNaiss); 
           int    age             = UtilDate.ageEnAnnees(pers.dateNaiss);
           
           System.out.printf(format,
                   
              pers.prenom,pers.nom, dateNaiss,age,pers.poids, pers.ville
           );        
        }  
    } 
    
    System.out.println();
   }
}



