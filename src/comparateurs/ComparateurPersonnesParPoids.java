
package comparateurs;

import java.util.Comparator;
import leclubdejudo.Personne;

public class ComparateurPersonnesParPoids implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Personne p  =  (Personne)   t ;
          Personne p1 = (Personne)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( p.poids   > p1.poids )   resultat =   1;
           else if  ( p.poids   < p1.poids )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}
