
package comparateurs;

import java.util.Comparator;
import leclubdejudo.Personne;

public class ComparateurPersonnesParNom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
         Personne p  = (Personne)  t ;
         Personne p1 = (Personne)  t1;
            
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
            resultat=  p.nom.compareTo(p1.nom);
            
            //</editor-fold>
        
       
        
        return resultat;  
    }    
}
