package utilitaires;

public class UtilDojo {
      
   public  static  String[] categories    = { "super-légers","mi-légers","légers",
                                              "mi-moyens","moyens",
                                              "mi-lourd","lourds"
                                            };
   
   public static String  determineCategorie(String sexe, int poids){
     
       return categories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   public static int     determineIndiceCategorie(String sexe, int poids){

      return (  sexe.equals("M") )? chercheIndice( poids, limitesHommes ) : chercheIndice( poids, limitesFemmes );
   }  
   
   public static  void   afficher(String pSexe){
         
       System.out.printf("%-15s Jusqu'à      %3d  kg\n", categories[0], limite(pSexe, 0));
        
       for (int i=1; i< categories.length-1;i++){
                            
           System.out.printf("%-15s de  %d  kg à %3d  kg\n",categories[i], limite(pSexe, i-1), limite(pSexe, i));     
       }
      
       int    indiceMax = categories.length-1;
       System.out.printf("%-15s A partir de  %3d  kg\n", categories[indiceMax],limite(pSexe,indiceMax-1)  
       );
   }
    
   //<editor-fold defaultstate="collapsed" desc="RESTE DU CODE">  
   
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }  
   
  
    private static int limite(String pSexe, int indice) {
        return (pSexe.equals("M")) ? limitesHommes[indice]-1:limitesFemmes[indice]-1;
    }
   
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   
   //</editor-fold>

   

}
   



